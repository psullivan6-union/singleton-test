import React from 'react';

export default ({ children }) => (
  <footer
    style={{ position: 'fixed', bottom: 0, width: '100% '}}
  >
    <h1>Footer things here</h1>
    {children}
  </footer>
);
