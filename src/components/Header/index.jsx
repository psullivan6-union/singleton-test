import React from 'react';
import { tongClassInstantiated } from '../../singleTong';

export default ({ children }) => (
  <header
    style={{ width: '100% '}}
  >
    <h1>Header things here</h1>
    <p>This is soooo contrived, since we're already calling these methods in the parent component AND passing them in as children. BUT, we're here to test, not create an amazing app.</p>
    <p>{ tongClassInstantiated.getValue() }</p>
    <p>{ tongClassInstantiated.getValue() }</p>
    { children }
  </header>
);
