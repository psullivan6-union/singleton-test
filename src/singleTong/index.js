export const tongSelfExecutingFunction = (() => {
  console.log('TONG - SELF EXECUTING FUNCTION');
  return 'When buying kitchen utensils, its best to purchase just 1 tong. A single tong, if you will.';
})();

export class tongClass {
  constructor() {
    // Added Math.random to force new console logs just for easier debugging
    console.log('TONG - CLASS', Math.random());
  }

  getValue() {
    return 'When buying kitchen utensils, its best to purchase just 1 tong. A single tong, if you will.';
  }
}

export const tongClassInstantiated = new tongClass();

export default {
  tongSelfExecutingFunction,
  tongClass,
};
