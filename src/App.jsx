import React, { Component } from 'react';
import './App.css';

// Components
import Header from './components/Header';
import Footer from './components/Footer';


import { tongSelfExecutingFunction, tongClass } from './singleTong';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header>
          <p>{ tongSelfExecutingFunction }</p>
          <p>{ new tongClass().getValue() }</p>
        </Header>
        <section className="Content">
          <h1>Body Content</h1>
          <p>{ tongSelfExecutingFunction }</p>
        </section>
        <Footer>
          <p>{ tongSelfExecutingFunction }</p>
          <p>{ new tongClass().getValue() }</p>
        </Footer>
      </div>
    );
  }
}

export default App;
